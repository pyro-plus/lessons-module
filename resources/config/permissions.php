<?php

return [
    'lessons' => [
        'read',
        'write',
        'delete',
    ],
    'courses' => [
        'read',
        'write',
        'delete',
    ],
];
