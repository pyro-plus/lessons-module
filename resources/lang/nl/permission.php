<?php

return [
    'lessons' => [
        'name'   => 'Lessons',
        'option' => [
            'read'   => 'Can read lessons?',
            'write'  => 'Can create/edit lessons?',
            'delete' => 'Can delete lessons?',
        ],
    ],
    'courses' => [
        'name'   => 'Courses',
        'option' => [
            'read'   => 'Can read courses?',
            'write'  => 'Can create/edit courses?',
            'delete' => 'Can delete courses?',
        ],
    ],
];
