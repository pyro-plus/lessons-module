<?php

return [
    'new_lesson' => 'Nieuwe les',
    'new_course' => 'Nieuwe training',
    'add_week'   => 'Nieuwe weekelijkse les',
];
