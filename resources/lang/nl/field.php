<?php

return [
    'name'        => [
        'name' => 'Naam',
    ],
    'description' => [
        'name' => 'Omschrijving',
    ],
    'start'       => [
        'name' => 'Start',
    ],
    'slug'        => [
        'name' => 'Slug',
    ],
    'members'     => [
        'name' => 'Deelnemers',
    ],
    'course'      => [
        'name' => 'Training',
    ],
    'max'         => [
        'name' => 'Maximaal aantal deelnemers',
    ],
    'duration'    => [
        'name' => 'Duur',
    ],
];
