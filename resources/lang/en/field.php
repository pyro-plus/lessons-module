<?php

return [
    'name'        => [
        'name' => 'Name',
    ],
    'description' => [
        'name' => 'Description',
    ],
    'start'       => [
        'name' => 'Start',
    ],
    'slug'        => [
        'name' => 'Slug',
    ],
    'members'     => [
        'name' => 'Members',
    ],
    'list'       => [
        'name' => 'Members List',
    ],
    'course'      => [
        'name' => 'Course',
    ],
    'max'         => [
        'name' => 'Max Users',
    ],
    'duration'    => [
        'name' => 'Duration',
    ],
];
