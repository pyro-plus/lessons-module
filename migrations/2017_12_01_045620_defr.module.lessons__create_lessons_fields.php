<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Anomaly\UsersModule\User\UserModel;
use Defr\LessonsModule\Course\CourseModel;

/**
 * Class DefrModuleLessonsCreateLessonsFields command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class DefrModuleLessonsCreateLessonsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name'        => 'anomaly.field_type.text',
        'description' => 'anomaly.field_type.wysiwyg',
        'start'       => 'anomaly.field_type.datetime',
        'image'       => 'anomaly.field_type.file',
        'slug'        => [
            'type'   => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type'    => '_',
            ],
        ],
        'members'     => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'related' => UserModel::class,
                'mode'    => 'lookup',
            ],
        ],
        'course'      => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => CourseModel::class,
            ],
        ],
        'max'         => [
            'type'   => 'anomaly.field_type.integer',
            'config' => [
                'min'           => 1,
                'default_value' => 5,
            ],
        ],
        'duration'    => [
            'type'   => 'anomaly.field_type.slider',
            'config' => [
                'min'           => 15,
                'max'           => 180,
                'step'          => 5,
                'default_value' => 45,
                'unit'          => 'minutes',
            ],
        ],
    ];

}
