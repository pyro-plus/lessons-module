<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

/**
 * Class DefrModuleLessonsCreateLessonsStream command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class DefrModuleLessonsCreateLessonsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'     => 'lessons',
        'order_by' => 'start',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'course'  => [
            'required' => true,
        ],
        'members',
        'start',
    ];

}
