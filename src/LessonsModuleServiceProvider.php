<?php namespace Defr\LessonsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\Lessons\LessonsCoursesEntryModel;
use Anomaly\Streams\Platform\Model\Lessons\LessonsLessonsEntryModel;
use Defr\LessonsModule\Course\Contract\CourseRepositoryInterface;
use Defr\LessonsModule\Course\CourseModel;
use Defr\LessonsModule\Course\CourseRepository;
use Defr\LessonsModule\Lesson\Contract\LessonRepositoryInterface;
use Defr\LessonsModule\Lesson\LessonModel;
use Defr\LessonsModule\Lesson\LessonRepository;

/**
 * Class LessonsModuleServiceProvider command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * The addon routes.
     *
     * @var array|null
     */
    protected $routes = [
        'admin/lessons'                   => 'Defr\LessonsModule\Http\Controller\Admin\LessonsController@index',
        'admin/lessons/choose'            => 'Defr\LessonsModule\Http\Controller\Admin\LessonsController@choose',
        'admin/lessons/create'            => 'Defr\LessonsModule\Http\Controller\Admin\LessonsController@create',
        'admin/lessons/edit/{id}'         => 'Defr\LessonsModule\Http\Controller\Admin\LessonsController@edit',
        'admin/lessons/week/choose'       => 'Defr\LessonsModule\Http\Controller\Admin\WeeklyController@choose',
        'admin/lessons/week/create'       => 'Defr\LessonsModule\Http\Controller\Admin\WeeklyController@create',
        'admin/lessons/courses'           => 'Defr\LessonsModule\Http\Controller\Admin\CoursesController@index',
        'admin/lessons/courses/create'    => 'Defr\LessonsModule\Http\Controller\Admin\CoursesController@create',
        'admin/lessons/courses/edit/{id}' => 'Defr\LessonsModule\Http\Controller\Admin\CoursesController@edit',
    ];

    /**
     * The addon class bindings.
     *
     * @var array|null
     */
    protected $bindings = [
        LessonsCoursesEntryModel::class => CourseModel::class,
        LessonsLessonsEntryModel::class => LessonModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @var array|null
     */
    protected $singletons = [
        CourseRepositoryInterface::class => CourseRepository::class,
        LessonRepositoryInterface::class => LessonRepository::class,
    ];
}
