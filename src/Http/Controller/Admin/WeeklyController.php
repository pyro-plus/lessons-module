<?php namespace Defr\LessonsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\LessonsModule\Course\Contract\CourseRepositoryInterface;
use Defr\LessonsModule\Lesson\Form\Command\AddCourseFromRequest;
use Defr\LessonsModule\Lesson\Form\WeeklyFormBuilder;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WeeklyController command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class WeeklyController extends AdminController
{

    /**
     * Choose a course for a batch creation
     *
     * @param  CourseRepositoryInterface $courses The courses
     * @return Response
     */
    public function choose(CourseRepositoryInterface $courses)
    {
        return view(
            'module::admin/lessons/week_choose',
            [
                'courses' => $courses->all(),
            ]
        );
    }

    /**
     * Create a batch of lessons
     *
     * @param  WeeklyFormBuilder $form The form
     * @return Response
     */
    public function create(WeeklyFormBuilder $form)
    {
        $this->dispatch(new AddCourseFromRequest($form));

        return $form->render();
    }
}
