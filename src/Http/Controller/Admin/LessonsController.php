<?php namespace Defr\LessonsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\LessonsModule\Course\Contract\CourseRepositoryInterface;
use Defr\LessonsModule\Lesson\Contract\LessonRepositoryInterface;
use Defr\LessonsModule\Lesson\Form\Command\AddCourseFromLesson;
use Defr\LessonsModule\Lesson\Form\Command\AddCourseFromRequest;
use Defr\LessonsModule\Lesson\Form\LessonFormBuilder;
use Defr\LessonsModule\Lesson\Table\LessonTableBuilder;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LessonsController command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  LessonTableBuilder $table
     * @return Response
     */
    public function index(LessonTableBuilder $table): Response
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  LessonFormBuilder $form
     * @return Response
     */
    public function create(LessonFormBuilder $form): Response
    {
        $this->dispatch(new AddCourseFromRequest($form));

        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  LessonRepositoryInterface $lessons The lessons repository
     * @param  LessonFormBuilder         $form    The form builder
     * @param  string                    $id      The identifier
     * @return Response
     */
    public function edit(
        LessonRepositoryInterface $lessons,
        LessonFormBuilder $form,
        $id
    ): Response {
        $lesson = $lessons->find($id);

        $this->dispatch(new AddCourseFromLesson($form, $lesson));

        return $form->render($id);
    }

    /**
     * Return the modal for choosing a course for a lesson.
     *
     * @param  CourseRepositoryInterface $courses
     * @return Response
     */
    public function choose(CourseRepositoryInterface $courses): Response
    {
        return view(
            'module::admin/lessons/choose',
            [
                'courses' => $courses->all(),
            ]
        );
    }
}
