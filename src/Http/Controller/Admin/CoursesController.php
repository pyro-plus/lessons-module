<?php namespace Defr\LessonsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\LessonsModule\Course\Form\CourseFormBuilder;
use Defr\LessonsModule\Course\Table\CourseTableBuilder;
use Symfony\Component\HttpFoundation\Response;

/**
 * Courses controller for the Admin CP
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class CoursesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  CourseTableBuilder $table The course table builder
     * @return Response
     */
    public function index(CourseTableBuilder $table): Response
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  CourseFormBuilder $form The course form builder
     * @return Response
     */
    public function create(CourseFormBuilder $form): Response
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  CourseFormBuilder $form The course form builder
     * @param  string            $id   The identifier
     * @return Response
     */
    public function edit(CourseFormBuilder $form, $id): Response
    {
        return $form->render($id);
    }
}
