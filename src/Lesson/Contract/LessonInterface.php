<?php namespace Defr\LessonsModule\Lesson\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Anomaly\UsersModule\User\UserCollection;
use Carbon\Carbon;
use Defr\LessonsModule\Course\Contract\CourseInterface;

interface LessonInterface extends EntryInterface
{

    /**
     * Gets the start.
     *
     * @return Carbon The start.
     */
    public function getStart(): Carbon;

    /**
     * Gets the maximum members.
     *
     * @return integer The maximum members count.
     */
    public function getMaxMembers();

    /**
     * Gets the duration.
     *
     * @return integer The duration of a lesson.
     */
    public function getDuration();

    /**
     * Gets the members.
     *
     * @return UserCollection The members.
     */
    public function getMembers(): UserCollection;

    /**
     * Gets the members count.
     *
     * @return integer The members count.
     */
    public function getMembersCount();

    /**
     * Adds a member to a lesson.
     *
     * @param  UserInterface $user The user
     * @return self
     */
    public function addMember(UserInterface $user);

    /**
     * Removes a member from a lesson.
     *
     * @param  UserInterface $user The user
     * @return self
     */
    public function removeMember(UserInterface $user);
    /**
     * Toggle a member to a lesson
     *
     * @param  UserInterface $user The user
     * @return self
     */
    public function toggleMember(UserInterface $user);

    /**
     * Determines if it has member.
     *
     * @param  UserInterface $user The user
     * @return boolean       True when has this member, false otherwise.
     */
    public function hasMember(UserInterface $user);

    /**
     * Gets the course.
     *
     * @return CourseInterface The course.
     */
    public function getCourse(): CourseInterface;

    /**
     * Determines if the lesson is full of members.
     *
     * @return boolean True when full, false otherwise.
     */
    public function isFull();
}
