<?php namespace Defr\LessonsModule\Lesson\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Carbon\Carbon;
use Defr\LessonsModule\Lesson\LessonCollection;

interface LessonRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find all lessons for week
     *
     * @param  Carbon             $date   The date
     * @param  mixed              $course The course
     * @return LessonCollection
     */
    public function findWeekByDateAndCourse(Carbon $date, $course): LessonCollection;

    /**
     * Find all lessons by day
     *
     * @param  Carbon             $date The date
     * @return LessonCollection
     */
    public function findAllByDate(Carbon $date): LessonCollection;

    /**
     * Find all lessons by day
     *
     * @param  Carbon             $date   The date
     * @param  mixed              $course The course
     * @return LessonCollection
     */
    public function findAllByDateAndCourse(Carbon $date, $course): LessonCollection;

    /**
     * Gets all lessons of defined user
     *
     * @param  UserInterface      $user The user
     * @return LessonCollection
     */
    public function findAllByUser(UserInterface $user): LessonCollection;
}
