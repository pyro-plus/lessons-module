<?php namespace Defr\LessonsModule\Lesson;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Carbon\Carbon;
use Defr\LessonsModule\Lesson\Contract\LessonRepositoryInterface;
use Defr\LessonsModule\Lesson\LessonCollection;

/**
 * Class LessonRepository
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonRepository extends EntryRepository implements LessonRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var LessonModel
     */
    protected $model;

    /**
     * Create a new LessonRepository instance.
     *
     * @param LessonModel $model The model
     */
    public function __construct(LessonModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find all lessons for week
     *
     * @param  Carbon             $date   The date
     * @param  mixed              $course The course
     * @return LessonCollection
     */
    public function findWeekByDateAndCourse(Carbon $date, $course): LessonCollection
    {
        if (is_object($course)) {
            $course = $course->getId();
        }

        return $this->model
            ->orderBy('start')
            ->where('course_id', $course)
            ->where('start', '>', $date->startOfWeek()->toDateTimeString())
            ->where('start', '<', $date->endOfWeek()->toDateTimeString())
            ->get();
    }

    /**
     * Find all lessons by day
     *
     * @param  Carbon             $date The date
     * @return LessonCollection
     */
    public function findAllByDate(Carbon $date): LessonCollection
    {
        return $this->model
            ->orderBy('start')
            ->where('start', '>', $date->startOfDay()->toDateTimeString())
            ->where('start', '<', $date->endOfDay()->toDateTimeString())
            ->get();
    }

    /**
     * Find all lessons by day
     *
     * @param  Carbon             $date   The date
     * @param  mixed              $course The course
     * @return LessonCollection
     */
    public function findAllByDateAndCourse(Carbon $date, $course): LessonCollection
    {
        return $this->model
            ->orderBy('start')
            ->where('course_id', $course)
            ->where('start', '>', $date->startOfDay()->toDateTimeString())
            ->where('start', '<', $date->endOfDay()->toDateTimeString())
            ->get();
    }

    /**
     * Gets all lessons of defined user
     *
     * @param  UserInterface      $user The user
     * @return LessonCollection
     */
    public function findAllByUser(UserInterface $user): LessonCollection
    {
        return $this->model
            ->orderBy('start')
            ->leftJoin(
                'lessons_lessons_members',
                'lessons_lessons.id',
                '=',
                'lessons_lessons_members.entry_id'
            )
            ->where('lessons_lessons_members.related_id', $user->getId())
            ->get();
    }
}
