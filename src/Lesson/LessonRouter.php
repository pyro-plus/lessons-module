<?php namespace Defr\LessonsModule\Lesson;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class LessonRouter command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class LessonRouter extends EntryRouter
{

}
