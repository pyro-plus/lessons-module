<?php namespace Defr\LessonsModule\Lesson\Command;

use Defr\LessonsModule\Lesson\Contract\LessonInterface;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class ToggleUserToLesson command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class ToggleUserToLesson
{

    /**
     * The lesson
     *
     * @var LessonInterface
     */
    protected $lesson;

    /**
     * Create a new instance of ToggleUserToLesson command
     *
     * @param LessonInterface $lesson The lesson
     */
    public function __construct(LessonInterface $lesson)
    {
        $this->lesson = $lesson;
    }

    /**
     * Handle the command
     *
     * @param  Guard             $auth The auth
     * @return LessonInterface
     */
    public function handle(Guard $auth): LessonInterface
    {
        if ($auth->guest()) {
            throw new \Exception('Only for registered users! Please log in or sign up.');
        }

        if ($this->lesson->getMembersCount() >= $this->lesson->course->getMax()) {
            throw new \Exception('There is full box');
        }

        return $this->lesson->toggleMember($auth->user());
    }
}
