<?php namespace Defr\LessonsModule\Lesson;

use Anomaly\Streams\Platform\Entry\EntryCollection;
use Carbon\Carbon;
use Defr\LessonsModule\Lesson\LessonPresenter;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class LessonCollection
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonCollection extends EntryCollection
{

    /**
     * Get lessons for day
     *
     * @param  string             $date The date.
     * @return LessonCollection
     */
    public function day(string $date): LessonCollection
    {
        return $this->filter(
            function (LessonPresenter $lesson) use ($date) {
                return $lesson->getStart()->format('Y-m-d') == $date;
            }
        );
    }

    /**
     * Get only future lessons
     *
     * @return LessonCollection
     */
    public function future(): LessonCollection
    {
        return $this->filter(
            function (LessonPresenter $lesson) {
                return $lesson->getStart()->gt(Carbon::now());
            }
        );
    }

    /**
     * Get only future lessons
     *
     * @param  integer            $months Months to get
     * @return LessonCollection
     */
    public function futureMonths($months = 3): LessonCollection
    {
        return $this->filter(
            function (LessonPresenter $lesson) use ($months) {
                $now = Carbon::now();

                return $lesson->getStart()->gt($now)
                && $lesson->getStart()->lt($now->addMonths($months));
            }
        );
    }

    /**
     * Get only past lessons
     *
     * @return LessonCollection
     */
    public function past(): LessonCollection
    {
        return $this->filter(
            function (LessonPresenter $lesson) {
                return $lesson->getStart()->lt(Carbon::now());
            }
        );
    }

    /**
     * Group lessons by date
     *
     * @return LessonCollection
     */
    public function byDate(): LessonCollection
    {
        return $this->sortBy(
            function (LessonPresenter $lesson) {
                return $lesson->getStart();
            }
        )->groupBy(
            function (LessonPresenter $lesson) {
                return substr($lesson->getStart(), 0, 10);
            }
        )->sortBy(
            function (LessonCollection $lessons, $key) {
                return $key;
            }
        );
    }

    /**
     * Group lessons by week
     *
     * @return LessonCollection
     */
    public function byWeek(): LessonCollection
    {
        return $this->byDate()->groupBy(
            function (LessonCollection $lessons, $key) {
                return Carbon::parse($key)->weekOfYear;
            }
        );
    }

    /**
     * Filter only lessons
     *
     * @return LessonCollection
     */
    public function ofUser(): LessonCollection
    {
        /* @var UserInterface $user */
        if (!$user = app(Guard::class)) {
            return $this;
        }

        return $this->filter(
            function (LessonPresenter $lesson) use ($user) {
                return $lesson->hasMember($user);
            }
        );
    }
}
