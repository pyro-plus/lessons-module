<?php namespace Defr\LessonsModule\Lesson;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\LessonsModule\Lesson\Contract\LessonRepositoryInterface;
use Illuminate\Support\Carbon;

/**
 * Class LessonSeeder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class LessonSeeder extends Seeder
{

    /**
     * The lessons repository
     *
     * @var LessonRepositoryInterface
     */
    protected $lessons;

    /**
     * Create an instance of LessonSeeder class
     *
     * @param LessonRepositoryInterface $lessons The lessons
     */
    public function __construct(LessonRepositoryInterface $lessons)
    {
        $this->lessons = $lessons;
    }

    /**
     * Run the seeder.
     *
     * @return void
     */
    public function run()
    {
        echo "\n\033[37;5;228mStarting lessons seeder!\n";

        $this->lessons->truncate();

        echo "\033[35;5;228mLessons truncated!\n";

        $data = [
            [
                'course_id' => 1,
                'start'     => Carbon::tomorrow()->setTime(9, 0),
            ],
            [
                'course_id' => 1,
                'start'     => Carbon::tomorrow()->setTime(9, 45),
            ],
            [
                'course_id' => 1,
                'start'     => Carbon::tomorrow()->setTime(10, 30),
            ],
            [
                'course_id' => 1,
                'start'     => Carbon::tomorrow()->setTime(11, 15),
            ],
            [
                'course_id' => 2,
                'start'     => Carbon::tomorrow()->setTime(9, 0),
            ],
            [
                'course_id' => 2,
                'start'     => Carbon::tomorrow()->setTime(10, 00),
            ],
            [
                'course_id' => 2,
                'start'     => Carbon::tomorrow()->setTime(11, 00),
            ],
            [
                'course_id' => 2,
                'start'     => Carbon::tomorrow()->setTime(12, 00),
            ],
            [
                'course_id' => 3,
                'start'     => Carbon::tomorrow()->setTime(9, 0),
            ],
            [
                'course_id' => 3,
                'start'     => Carbon::tomorrow()->setTime(10, 30),
            ],
            [
                'course_id' => 3,
                'start'     => Carbon::tomorrow()->setTime(12, 00),
            ],
            [
                'course_id' => 3,
                'start'     => Carbon::tomorrow()->setTime(13, 30),
            ],
        ];

        foreach ($data as $lesson) {
            $courseId = array_get($lesson, 'course_id');
            $start    = array_get($lesson, 'start');

            $this->lessons->create($lesson)->members()->sync([1]);

            echo "\033[36;5;228mCreated lesson for \033[31;5;228m{$courseId}"
                . " \033[36;5;228mcourse. \033[31;5;228m{$start}\n";
        }

        echo "\033[32;5;228mLessons was seeded successfully!\n\n";
    }
}
