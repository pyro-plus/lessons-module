<?php namespace Defr\LessonsModule\Lesson;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class LessonPresenter command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class LessonPresenter extends EntryPresenter
{

}
