<?php namespace Defr\LessonsModule\Lesson\Form;

/**
 * Class WeeklyFormBuilder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class WeeklyFormBuilder extends LessonFormBuilder
{

    /**
     * Form fields
     *
     * @var array
     */
    protected $fields = [
        'start'  => [
            'label'  => 'Start',
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'default_value' => 'now',
                'mode'          => 'date',
                'plugin_format' => 'j F, Y',
                'date_format'   => 'j F, Y',
                'year_range'    => '-50:+50',
            ],
        ],
        'mapper' => 'defr.field_type.lessons_mapper',
    ];

    /**
     * On form was built
     *
     * @return void
     */
    public function onBuilt()
    {

    }
}
