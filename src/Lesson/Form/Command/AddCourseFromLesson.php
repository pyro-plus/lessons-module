<?php namespace Defr\LessonsModule\Lesson\Form\Command;

use Defr\LessonsModule\Lesson\Contract\LessonInterface;
use Defr\LessonsModule\Lesson\Form\LessonFormBuilder;

/**
 * Class AddCourseFromLesson command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class AddCourseFromLesson
{

    /**
     * Form builder
     *
     * @var LessonFormBuilder
     */
    protected $builder;

    /**
     * Lesson model
     *
     * @var LessonInterface
     */
    protected $lesson;

    /**
     * Create an instance of AddCourseFromLesson command
     *
     * @param LessonFormBuilder $builder The builder
     * @param LessonInterface   $lesson  The lesson
     */
    public function __construct(
        LessonFormBuilder $builder,
        LessonInterface $lesson
    ) {
        $this->builder = $builder;
        $this->lesson  = $lesson;
    }

    /**
     * Handle the command
     *
     * @return void
     */
    public function handle()
    {
        $this->builder->setCourse($this->lesson->getCourse());
    }
}
