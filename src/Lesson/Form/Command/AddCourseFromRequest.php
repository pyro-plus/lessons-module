<?php namespace Defr\LessonsModule\Lesson\Form\Command;

use Defr\LessonsModule\Course\Contract\CourseRepositoryInterface;
use Defr\LessonsModule\Lesson\Form\LessonFormBuilder;
use Illuminate\Http\Request;

/**
 * Class AddCourseFromRequest command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class AddCourseFromRequest
{

    /**
     * Form builder
     *
     * @var LessonFormBuilder
     */
    protected $builder;

    /**
     * Create an instance of AddCourseFromRequest command
     *
     * @param LessonFormBuilder $builder The builder
     */
    public function __construct(LessonFormBuilder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Handle the command
     *
     * @param  CourseRepositoryInterface $courses The courses
     * @param  Request                   $request The request
     * @return void
     */
    public function handle(CourseRepositoryInterface $courses, Request $request)
    {
        if (!$id = $request->get('course')) {
            return false;
        }

        $this->builder->setCourse($courses->find($id));
    }
}
