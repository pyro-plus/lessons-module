<?php namespace Defr\LessonsModule\Lesson\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Defr\LessonsModule\Course\Contract\CourseInterface;

/**
 * Class LessonFormBuilder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonFormBuilder extends FormBuilder
{

    /**
     * The course instance
     *
     * @var CourseInterface
     */
    protected $course;

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [
        'course',
    ];

    /**
     * Fired when the builder is ready to build.
     *
     * @throws Exception
     * @return void
     */
    public function onReady()
    {
        if (!$this->getCourse()) {
            throw new \Exception(
                'The $course parameter is required when creating a lesson.'
            );
        }
    }

    /**
     * On form was built
     *
     * @return void
     */
    public function onBuilt()
    {
        //$this->getFormField('members')->config('max', $this->course->getMax());
    }

    /**
     * Fired just before saving the form.
     *
     * @return void
     */
    public function onSaving()
    {
        $entry  = $this->getFormEntry();
        $course = $this->getCourse();

        if (!$entry->course_id) {
            $entry->course_id = $course->getId();
        }
    }

    /**
     * Gets the course.
     *
     * @return CourseInterface The course.
     */
    public function getCourse(): CourseInterface
    {
        return $this->course;
    }

    /**
     * Sets the course.
     *
     * @param  CourseInterface $course The course
     * @return self
     */
    public function setCourse(CourseInterface $course): self
    {
        $this->course = $course;

        return $this;
    }
}
