<?php namespace Defr\LessonsModule\Lesson\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class LessonTableBuilder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonTableBuilder extends TableBuilder
{

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'course',
        'start',
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'name'     => [
            'value' => 'entry.course.name',
        ],
        'start',
        'duration' => [
            'value' => 'entry.course.duration',
        ],
        'members'  => [
            'wrapper' => '{value.current} of {value.max}',
            'value'   => [
                'current' => 'entry.members.count',
                'max'     => 'entry.course.max',
            ],
        ],
        'list'     => [
            'wrapper' => '<ul class="members-block-{entry.id} collapse"><li>{value}</li></ul>',
            'value'   => 'entry.members.pluck("display_name.value").implode("</li><li>")',
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'show' => [
            'type'        => 'info',
            'text'        => 'defr.module.lessons::button.show_members',
            'href'        => '#',
            'data-toggle' => 'collapse',
            'data-target' => '.members-block-{entry.id}',
        ],
        'edit',
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete',
    ];
}
