<?php namespace Defr\LessonsModule\Lesson;

use Anomaly\Streams\Platform\Model\Lessons\LessonsLessonsEntryModel;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Anomaly\UsersModule\User\UserCollection;
use Carbon\Carbon;
use Defr\LessonsModule\Course\Contract\CourseInterface;
use Defr\LessonsModule\Lesson\Contract\LessonInterface;

/**
 * Class LessonModel
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonModel extends LessonsLessonsEntryModel implements LessonInterface
{

    /**
     * Eager relations
     *
     * @var null|array
     */
    protected $with = [
        'members',
    ];

    // /**
    //  * Cascaded relations
    //  *
    //  * @var null|array
    //  */
    // protected $cascades = [
    //     'members',
    // ];

    /**
     * Gets the start.
     *
     * @return Carbon The start.
     */
    public function getStart(): Carbon
    {
        return $this->start;
    }

    /**
     * Gets the maximum members.
     *
     * @return integer The maximum members count.
     */
    public function getMaxMembers(): int
    {
        return $this->course->getMax();
    }

    /**
     * Gets the duration.
     *
     * @return integer The duration of a lesson.
     */
    public function getDuration(): int
    {
        return (integer) $this->course->getDuration();
    }

    /**
     * Gets the members.
     *
     * @return UserCollection The members.
     */
    public function getMembers(): UserCollection
    {
        return $this->members;
    }

    /**
     * Gets the members count.
     *
     * @return integer The members count.
     */
    public function getMembersCount(): int
    {
        return $this->members->count();
    }

    /**
     * Adds a member to a lesson.
     *
     * @param  UserInterface $user The user
     * @return self
     */
    public function addMember(UserInterface $user): self
    {
        if ($this->getMembersCount() < $this->getMaxMembers()) {
            $this->members()->attach($user->getId());
        }

        return $this;
    }

    /**
     * Removes a member from a lesson.
     *
     * @param  UserInterface $user The user
     * @return self
     */
    public function removeMember(UserInterface $user): self
    {
        $this->members()->detach($user->getId());

        return $this;
    }

    /**
     * Toggle a member to a lesson
     *
     * @param  UserInterface $user The user
     * @return self
     */
    public function toggleMember(UserInterface $user): self
    {
        if ($this->hasMember($user)) {
            return $this->removeMember($user);
        }

        return $this->addMember($user);
    }

    /**
     * Determines if it has member.
     *
     * @param  UserInterface $user The user
     * @return boolean       True when has this member, false otherwise.
     */
    public function hasMember(UserInterface $user): bool
    {
        return (boolean) $this->members->where('id', $user->getId())->first();
    }

    /**
     * Gets the course.
     *
     * @return CourseInterface The course.
     */
    public function getCourse(): CourseInterface
    {
        return $this->course;
    }

    /**
     * Determines if the lesson is full of members.
     *
     * @return boolean True when full, false otherwise.
     */
    public function isFull(): bool
    {
        return $this->getMembersCount() >= $this->getMaxMembers();
    }
}
