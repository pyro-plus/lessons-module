<?php namespace Defr\LessonsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class LessonsModule command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonsModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var boolean
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-graduation-cap';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'lessons' => [
            'buttons' => [
                'new_lesson' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'href'        => 'admin/lessons/choose',
                ],
                'add_week'   => [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'href'        => 'admin/lessons/week/choose',
                ],
            ],
        ],
        'courses' => [
            'buttons' => [
                'new_course',
            ],
        ],
    ];
}
