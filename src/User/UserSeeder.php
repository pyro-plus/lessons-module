<?php namespace Defr\LessonsModule\User;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;

/**
 * Class UserSeeder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class UserSeeder extends Seeder
{

    /**
     * Users repository
     *
     * @var UserRepositoryInterface
     */
    protected $users;

    /**
     * Create an instance of UserSeeder class
     *
     * @param UserRepositoryInterface $users The users
     */
    public function __construct(UserRepositoryInterface $users)
    {
        $this->users = $users;
    }

    /**
     * Run the seeder
     */
    public function run()
    {
        echo "\n\033[37;5;228mStarting users seeder!\n";

        if (!$users = json_decode(
            file_get_contents(__DIR__ . '/../../resources/seeder/users.json'),
            true
        )) {
            throw new \Exception('JSON error in `users.json` file');
        }

        foreach ($users as $user) {
            $firstname   = array_get($user, 'first_name');
            $lastname    = array_get($user, 'last_name');
            $displayname = "{$firstname} {$lastname}";

            array_set($user, 'display_name', $displayname);
            array_set($user, 'username', str_slug($displayname));
            array_set($user, 'password', str_random(16));
            array_set($user, 'roles', [2]);

            $newUser = $this->users->create($user);

            echo "\033[36;5;228mCreated user \033[31;5;228m{$newUser->getEmail()}\n";
        }

        echo "\033[32;5;228mAll users was seeded successfully!\n";
    }
}
