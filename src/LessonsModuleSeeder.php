<?php namespace Defr\LessonsModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\LessonsModule\Course\CourseSeeder;
use Defr\LessonsModule\Lesson\LessonSeeder;

// use Defr\LessonsModule\User\UserSeeder;

/**
 * Class LessonsModuleSeeder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class LessonsModuleSeeder extends Seeder
{

    /**
     * Run the seeder
     *
     * @return void
     */
    public function run()
    {
        $this->call(CourseSeeder::class);
        $this->call(LessonSeeder::class);
        // $this->call(UserSeeder::class);
    }
}
