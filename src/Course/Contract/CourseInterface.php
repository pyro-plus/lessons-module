<?php namespace Defr\LessonsModule\Course\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\LessonsModule\Course\Contract\CourseInterface;
use Defr\LessonsModule\Lesson\LessonCollection;
use Illuminate\Database\Eloquent\Relations\HasMany;

interface CourseInterface extends EntryInterface
{

    /**
     * Lessons relation
     *
     * @return HasMany The relation.
     */
    public function lessons(): HasMany;

    /**
     * Gets the lessons.
     *
     * @return LessonCollection The lessons.
     */
    public function getLessons(): LessonCollection;

    /**
     * Gets the maximum of students.
     *
     * @return integer The maximum.
     */
    public function getMax();

    /**
     * Gets the duration.
     *
     * @return string The duration.
     */
    public function getDuration();
}
