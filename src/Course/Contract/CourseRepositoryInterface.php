<?php namespace Defr\LessonsModule\Course\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;
use Defr\LessonsModule\Course\Contract\CourseInterface;
use Defr\LessonsModule\Course\CourseCollection;

interface CourseRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find course by its slug
     *
     * @param  string            $slug The slug
     * @return CourseInterface
     */
    public function findBySlug($slug): CourseInterface;

    /**
     * Find courses by day
     *
     * @param  string             $date The date
     * @return CourseCollection
     */
    public function findAllByDate($date): CourseCollection;
}
