<?php namespace Defr\LessonsModule\Course;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class CourseRouter command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class CourseRouter extends EntryRouter
{

}
