<?php namespace Defr\LessonsModule\Course;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class CourseObserver command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class CourseObserver extends EntryObserver
{

}
