<?php namespace Defr\LessonsModule\Course;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\LessonsModule\Course\Contract\CourseRepositoryInterface;

/**
 * Class CourseSeeder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class CourseSeeder extends Seeder
{

    /**
     * The course instance
     *
     * @var CourseRepositoryInterface
     */
    protected $courses;

    /**
     * Create an instance of a class
     *
     * @param CourseRepositoryInterface $courses The courses
     */
    public function __construct(CourseRepositoryInterface $courses)
    {
        $this->courses = $courses;
    }

    /**
     * Run the seeder.
     *
     * @return void
     */
    public function run()
    {
        echo "\n\033[37;5;228mStarting courses seeder!\n";

        $this->courses->truncate();

        echo "\033[35;5;228mCourses truncated!\n";

        $data = [
            [
                'en'       => [
                    'name'        => 'Lesson A',
                    'description' => 'Lesson A short description',
                ],
                'slug'     => 'lesson-a',
                'duration' => 45,
                'max'      => 4,
            ],
            [
                'en'       => [
                    'name'        => 'Lesson B',
                    'description' => 'Lesson B short description',
                ],
                'slug'     => 'lesson-b',
                'duration' => 60,
                'max'      => 5,
            ],
            [
                'en'       => [
                    'name'        => 'Lesson C',
                    'description' => 'Lesson C short description',
                ],
                'slug'     => 'lesson-c',
                'duration' => 90,
                'max'      => 7,
            ],
        ];

        foreach ($data as $course) {
            $name = array_get($course, 'en.name');
            $slug = array_get($course, 'slug');

            $this->courses->create($course);

            echo "\033[36;5;228mCreated course \033[31;5;228m{$name}. Slug: {$slug}\n";
        }

        echo "\033[32;5;228mCourses was seeded successfully!\n\n";
    }
}
