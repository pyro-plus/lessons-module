<?php namespace Defr\LessonsModule\Course\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class CourseFormBuilder command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class CourseFormBuilder extends FormBuilder
{

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'cancel',
    ];
}
