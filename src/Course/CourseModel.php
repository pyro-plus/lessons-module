<?php namespace Defr\LessonsModule\Course;

use Anomaly\Streams\Platform\Model\Lessons\LessonsCoursesEntryModel;
use Defr\LessonsModule\Course\Contract\CourseInterface;
use Defr\LessonsModule\Lesson\LessonModel;
use Defr\LessonsModule\Lesson\LessonCollection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class CourseModel command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 *
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://pyrocms.com
 */
class CourseModel extends LessonsCoursesEntryModel implements CourseInterface
{

    /**
     * Eager relations
     *
     * @var null|array
     */
    protected $with = [
        'lessons',
    ];

    /**
     * Cascaded relations
     *
     * @var null|array
     */
    protected $cascades = [
        'lessons',
    ];

    /**
     * Lessons relation
     *
     * @return HasMany The relation.
     */
    public function lessons(): HasMany
    {
        return $this->hasMany(LessonModel::class, 'course_id', 'id');
    }

    /**
     * Gets the lessons.
     *
     * @return LessonCollection The lessons.
     */
    public function getLessons(): LessonCollection
    {
        return $this->lessons;
    }

    /**
     * Gets the maximum of students.
     *
     * @return integer The maximum.
     */
    public function getMax(): int
    {
        return (integer) $this->max;
    }

    /**
     * Gets the duration.
     *
     * @return string The duration.
     */
    public function getDuration(): str
    {
        return $this->duration;
    }
}
