<?php namespace Defr\LessonsModule\Course;

use Defr\LessonsModule\Course\Contract\CourseInterface;
use Defr\LessonsModule\Course\Contract\CourseRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class CourseRepository command
 *
 * @category Streams_Platform_Addon
 * @package  LessonsModule
 * @author   Denis Efremov <efremov.a.denis@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://pyrocms.com
 */
class CourseRepository extends EntryRepository implements CourseRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var CourseModel
     */
    protected $model;

    /**
     * Create a new CourseRepository instance.
     *
     * @param CourseModel $model
     */
    public function __construct(CourseModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find course by its slug
     *
     * @param string $slug The slug
     *
     * @return CourseInterface
     */
    public function findBySlug($slug): CourseInterface
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
     * Find courses by day
     *
     * @param string $date The date
     *
     * @return CourseCollection
     */
    public function findAllByDate($date): CourseCollection
    {
        return $this->model->all()->filter(
            function (CourseInterface $course) use ($date) {
                /** @var LessonCollection $lessons */
                $lessons = $course->getLessons()->day($date);

                return $lessons->count() > 0 ? $lessons : false;
            }
        );
    }
}
